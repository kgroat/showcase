-- every hour, delete rooms older than 1 day
select
  cron.schedule(
    'clear-old-rooms', -- name of the cron job
    '0 * * * *', -- run every hour
    $$
      delete from public.planning_poker_room
        where created_at < now() - interval '1 day' -- delete rooms older than 1 day
    $$
  );

-- drop old replication which is probably empty
drop publication if exists supabase_realtime; 

-- enable realtime replication for the given tables
create publication supabase_realtime for
	table public.planning_poker_room,
	table public.planning_poker_room_member;
