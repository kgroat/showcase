
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

CREATE EXTENSION IF NOT EXISTS "pgsodium" WITH SCHEMA "pgsodium";

CREATE EXTENSION IF NOT EXISTS "pg_graphql" WITH SCHEMA "graphql";

CREATE EXTENSION IF NOT EXISTS "pg_stat_statements" WITH SCHEMA "extensions";

CREATE EXTENSION IF NOT EXISTS "pgcrypto" WITH SCHEMA "extensions";

CREATE EXTENSION IF NOT EXISTS "pgjwt" WITH SCHEMA "extensions";

CREATE EXTENSION IF NOT EXISTS "supabase_vault" WITH SCHEMA "vault";

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA "extensions";

CREATE OR REPLACE FUNCTION "public"."user_has_membership"("target_user_id" "uuid", "target_room_id" "uuid") RETURNS boolean
    LANGUAGE "plpgsql" SECURITY DEFINER
    AS $$BEGIN
IF (target_user_id = auth.uid()) THEN
  RETURN TRUE;
END IF;
RETURN EXISTS (
  SELECT id
  FROM planning_poker_room_member
  WHERE room_id = target_room_id
  AND user_id = auth.uid()
);
END;
$$;

ALTER FUNCTION "public"."user_has_membership"("target_user_id" "uuid", "target_room_id" "uuid") OWNER TO "postgres";

CREATE OR REPLACE FUNCTION "public"."user_has_membership2"("target_user_id" "uuid", "target_room_id" "uuid") RETURNS boolean
    LANGUAGE "plpgsql" STABLE
    AS $$BEGIN
IF (target_user_id = auth.uid()) THEN
  RETURN TRUE;
END IF;
RETURN EXISTS (
  SELECT id
  FROM planning_poker_room_member
  WHERE room_id = target_room_id
  AND user_id = auth.uid()
);
END;
$$;

ALTER FUNCTION "public"."user_has_membership2"("target_user_id" "uuid", "target_room_id" "uuid") OWNER TO "postgres";

SET default_tablespace = '';

SET default_table_access_method = "heap";

CREATE TABLE IF NOT EXISTS "public"."planning_poker_room" (
    "id" "uuid" DEFAULT "gen_random_uuid"() NOT NULL,
    "name" character varying NOT NULL,
    "facilitator_id" "uuid" NOT NULL,
    "votes_visible" boolean DEFAULT false NOT NULL,
    "created_at" timestamp with time zone DEFAULT ("now"() AT TIME ZONE 'utc'::"text")
);

ALTER TABLE "public"."planning_poker_room" OWNER TO "postgres";

CREATE TABLE IF NOT EXISTS "public"."planning_poker_room_member" (
    "id" "uuid" DEFAULT "gen_random_uuid"() NOT NULL,
    "room_id" "uuid" NOT NULL,
    "user_id" "uuid" NOT NULL,
    "vote" character varying
);

ALTER TABLE "public"."planning_poker_room_member" OWNER TO "postgres";

ALTER TABLE ONLY "public"."planning_poker_room_member"
    ADD CONSTRAINT "planning_poker_room_members_pkey" PRIMARY KEY ("id");

ALTER TABLE ONLY "public"."planning_poker_room"
    ADD CONSTRAINT "planning_poker_room_pkey" PRIMARY KEY ("id");

CREATE UNIQUE INDEX "planning_poker_room_member_room_id_user_id_idx" ON "public"."planning_poker_room_member" USING "btree" ("room_id", "user_id");

CREATE INDEX "planning_poker_room_members_room_id_idx" ON "public"."planning_poker_room_member" USING "btree" ("room_id");

ALTER TABLE ONLY "public"."planning_poker_room"
    ADD CONSTRAINT "planning_poker_room_facilitator_id_fkey" FOREIGN KEY ("facilitator_id") REFERENCES "auth"."users"("id") ON DELETE CASCADE;

ALTER TABLE ONLY "public"."planning_poker_room_member"
    ADD CONSTRAINT "planning_poker_room_member_room_id_fkey" FOREIGN KEY ("room_id") REFERENCES "public"."planning_poker_room"("id") ON DELETE CASCADE;

ALTER TABLE ONLY "public"."planning_poker_room_member"
    ADD CONSTRAINT "planning_poker_room_member_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "auth"."users"("id") ON DELETE CASCADE;

CREATE POLICY "Insert member with user_id" ON "public"."planning_poker_room_member" FOR INSERT WITH CHECK (("auth"."uid"() = "user_id"));

CREATE POLICY "Insert room with facilitator_id" ON "public"."planning_poker_room" FOR INSERT WITH CHECK (("auth"."uid"() = "facilitator_id"));

CREATE POLICY "Select members if user has joined room" ON "public"."planning_poker_room_member" FOR SELECT USING ((("user_id" = "auth"."uid"()) OR "public"."user_has_membership"("user_id", "room_id")));

CREATE POLICY "Select room if user has joined room or is facilitator" ON "public"."planning_poker_room" FOR SELECT USING ((("auth"."uid"() = "facilitator_id") OR (EXISTS ( SELECT "planning_poker_room_member"."id"
   FROM "public"."planning_poker_room_member"
  WHERE (("planning_poker_room"."id" = "planning_poker_room_member"."room_id") AND ("planning_poker_room_member"."user_id" = "auth"."uid"()))))));

CREATE POLICY "Update if facilitator or user" ON "public"."planning_poker_room_member" FOR UPDATE USING ((("user_id" = "auth"."uid"()) OR (EXISTS ( SELECT "ppr"."id",
    "ppr"."name",
    "ppr"."facilitator_id",
    "ppr"."votes_visible"
   FROM "public"."planning_poker_room" "ppr"
  WHERE (("ppr"."facilitator_id" = "auth"."uid"()) AND ("ppr"."id" = "planning_poker_room_member"."room_id")))))) WITH CHECK ((("user_id" = "auth"."uid"()) OR (EXISTS ( SELECT "ppr"."id",
    "ppr"."name",
    "ppr"."facilitator_id",
    "ppr"."votes_visible"
   FROM "public"."planning_poker_room" "ppr"
  WHERE (("ppr"."facilitator_id" = "auth"."uid"()) AND ("ppr"."id" = "planning_poker_room_member"."room_id"))))));

CREATE POLICY "Update room with facilitator_id" ON "public"."planning_poker_room" FOR UPDATE USING (("auth"."uid"() = "facilitator_id")) WITH CHECK (("auth"."uid"() = "facilitator_id"));

ALTER TABLE "public"."planning_poker_room" ENABLE ROW LEVEL SECURITY;

ALTER TABLE "public"."planning_poker_room_member" ENABLE ROW LEVEL SECURITY;

REVOKE USAGE ON SCHEMA "public" FROM PUBLIC;
GRANT USAGE ON SCHEMA "public" TO "postgres";
GRANT USAGE ON SCHEMA "public" TO "anon";
GRANT USAGE ON SCHEMA "public" TO "authenticated";
GRANT USAGE ON SCHEMA "public" TO "service_role";

GRANT ALL ON FUNCTION "public"."user_has_membership"("target_user_id" "uuid", "target_room_id" "uuid") TO "anon";
GRANT ALL ON FUNCTION "public"."user_has_membership"("target_user_id" "uuid", "target_room_id" "uuid") TO "authenticated";
GRANT ALL ON FUNCTION "public"."user_has_membership"("target_user_id" "uuid", "target_room_id" "uuid") TO "service_role";

GRANT ALL ON FUNCTION "public"."user_has_membership2"("target_user_id" "uuid", "target_room_id" "uuid") TO "anon";
GRANT ALL ON FUNCTION "public"."user_has_membership2"("target_user_id" "uuid", "target_room_id" "uuid") TO "authenticated";
GRANT ALL ON FUNCTION "public"."user_has_membership2"("target_user_id" "uuid", "target_room_id" "uuid") TO "service_role";

GRANT ALL ON TABLE "public"."planning_poker_room" TO "anon";
GRANT ALL ON TABLE "public"."planning_poker_room" TO "authenticated";
GRANT ALL ON TABLE "public"."planning_poker_room" TO "service_role";

GRANT ALL ON TABLE "public"."planning_poker_room_member" TO "anon";
GRANT ALL ON TABLE "public"."planning_poker_room_member" TO "authenticated";
GRANT ALL ON TABLE "public"."planning_poker_room_member" TO "service_role";

ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON SEQUENCES  TO "postgres";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON SEQUENCES  TO "anon";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON SEQUENCES  TO "authenticated";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON SEQUENCES  TO "service_role";

ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON FUNCTIONS  TO "postgres";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON FUNCTIONS  TO "anon";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON FUNCTIONS  TO "authenticated";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON FUNCTIONS  TO "service_role";

ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON TABLES  TO "postgres";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON TABLES  TO "anon";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON TABLES  TO "authenticated";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON TABLES  TO "service_role";

RESET ALL;
