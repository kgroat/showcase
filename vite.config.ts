import { qwikVite } from '@builder.io/qwik/optimizer'
import { qwikCity } from '@builder.io/qwik-city/vite'
import { join } from 'path'
import { defineConfig } from 'vite'
import tsconfigPaths from 'vite-tsconfig-paths'

export default defineConfig(() => {
  return {
    plugins: [qwikCity(), qwikVite(), tsconfigPaths()],
    preview: {
      headers: {
        'Cache-Control': 'public, max-age=600',
      },
    },
    resolve: {
      alias: {
        // Required to `@import` from `~/styles` in css files
        '~/styles': join(__dirname, 'src', 'styles'),
      },
    },
    optimizeDeps: {
      /**
       * Necessary to fix cypress tests in CI. If you see a test flake with a message in the terminal like
       * [vite] ✨ new dependencies optimized: <some-dependency>
       * then you should add that dependency here.
       */
      include: [
        '@supabase/supabase-js',
        'check-password-strength',
      ],
    },
  }
})
