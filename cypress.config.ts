import { addCucumberPreprocessorPlugin } from '@badeball/cypress-cucumber-preprocessor'
import webpackPreprocessor from '@cypress/webpack-preprocessor'
import { configureAllureAdapterPlugins } from '@mmisty/cypress-allure-adapter/plugins'
import { defineConfig } from 'cypress'
import { join } from 'path'
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin'

const REQUIRED_ENV = ['localAuthKey']

let env: any
try {
  env = require('./cypress.env.json')

  REQUIRED_ENV.forEach((key) => {
    if (!env[key]) {
      console.warn(`Your cypress.env.json does not contain a required key: "${key}"`)
    }
  })
} catch (e) {
  console.warn('You do not have a cypress.env.json file')
}

async function setupNodeEvents (
  on: Cypress.PluginEvents,
  config: Cypress.PluginConfigOptions,
): Promise<Cypress.PluginConfigOptions> {
  // Set omitAfterSpecHandler because the allure adapter creates its own `after:spec` handler
  await addCucumberPreprocessorPlugin(on, config, { omitAfterSpecHandler: true })
  configureAllureAdapterPlugins(on, config)

  on(
    'file:preprocessor',
    webpackPreprocessor({
      webpackOptions: {
        devtool: 'cheap-eval-source-map',
        resolve: {
          extensions: ['.ts', '.js'],
          plugins: [new TsconfigPathsPlugin()],
        },
        module: {
          rules: [
            {
              test: /\.ts$/,
              exclude: [/node_modules/],
              use: [
                {
                  loader: 'ts-loader',
                  options: {
                    configFile: join(__dirname, 'cypress.tsconfig.json'),
                  },
                },
              ],
            },
            {
              test: /\.feature$/,
              use: [
                {
                  loader: '@badeball/cypress-cucumber-preprocessor/webpack',
                  options: config,
                },
              ],
            },
          ],
        },
      },
    }),
  )

  // Make sure to return the config object as it might have been modified by the plugin.
  return config
}

export default defineConfig({
  projectId: 'qsx7gi',
  e2e: {
    setupNodeEvents,
    experimentalOriginDependencies: true,
    specPattern: ['src/**/*.feature'],
    baseUrl: 'http://localhost:5173',
    env: {
      // allure config
      allure: true,
      allureCleanResults: true,

      // supabase config
      supabaseApiPort: 54321,
      supabaseUiPort: 54323,
      supabaseInbucketPort: 54324,
    },
    video: true,
    defaultCommandTimeout: 10000,
    retries: {
      runMode: 2,
    },
  },
})
