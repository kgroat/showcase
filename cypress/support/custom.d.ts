/// <reference types="@testing-library/cypress" />

declare namespace Cypress {
  interface E2eContext {
    email?: string
    password?: string
  }

  interface Chainable {
    supabase (callback: () => void): void
    supabase <T>(args: T, callback: (args: T) => void): void

    createUser (email: string, password: string): void
  }
}

declare namespace Mocha {
  interface Context {
    e2eCtx?: Cypress.E2eContext
  }
}
