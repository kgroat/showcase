/// <reference types="cypress" />
import '@testing-library/cypress/add-commands'

Cypress.Commands.add('supabase', function (args: any | (() => void), callback?: (args: any) => void) {
  const supabaseUi = `http://localhost:${Cypress.env('supabaseUiPort')}`

  const log = Cypress.log({
    displayName: 'supabase',
    autoEnd: false,
  })

  if (typeof args === 'function') {
    callback = args
    args = {}
  }

  cy.origin(supabaseUi, () => {
    if (typeof cy.findByTestId === 'undefined') {
      Cypress.require('./commands')
    }
  })

  cy.origin(supabaseUi, { args }, callback!).then(() => log.end())
})

Cypress.Commands.add('createUser', function (email, password) {
  const consoleProps: any = {
    email,
    password,
  }

  const log = Cypress.log({
    displayName: 'createUser',
    message: [email, password],
    autoEnd: false,
    consoleProps: () => consoleProps,
  })

  cy.request({
    method: 'POST',
    url: `http://localhost:${Cypress.env('supabaseApiPort')}/auth/v1/admin/users`,
    body: {
      email,
      password,
      email_confirm: true,
    },
    headers: {
      Authorization: `Bearer ${Cypress.env('localAuthKey')}`,
    },
    log: false,
  }).then((data) => {
    consoleProps.user = data.body
    log.end()
  })
})
