import { type DataTable, Given, Step, Then } from '@badeball/cypress-cucumber-preprocessor'

import { generateEmail } from '~/test-utils/email'

Given('I am logged in', function (data?: DataTable) {
  const rows = data?.rowsHash()
  const create = rows?.create ?? 'true'

  if (create === 'true') {
    Step(this, 'I have a user account')
  }

  Step(this, 'I am on the login page')
  Step(this, 'I enter my login details')
})

Given('I have a user account', function () {
  const email = generateEmail()
  const password = 'Insecure P@ssw0rd'
  this.e2eCtx = {
    ...this.e2eCtx,
    email,
    password,
  }

  cy.createUser(email, password)
})

Then('I should be logged in', function () {
  cy.findByTestId('greeting').should('be.visible')
})
