import { Then } from '@badeball/cypress-cucumber-preprocessor'

Then('I should see a {string} form error', function (error: string) {
  cy.findAllByTestId('form-error')
    .contains(error)
    .should('be.visible')
})
