import { After, Before } from '@badeball/cypress-cucumber-preprocessor'

const LAST_USER_EMAIL = 'lastUserEmail'
const LAST_USER_PASSWORD = 'lastUserPassword'

Before({ tags: '@keep-user' }, function () {
  this.e2eCtx = {
    ...this.e2eCtx,
    email: Cypress.env(LAST_USER_EMAIL),
    password: Cypress.env(LAST_USER_PASSWORD),
  }
})

After(function () {
  if (this.e2eCtx?.email) {
    Cypress.env(LAST_USER_EMAIL, this.e2eCtx.email)
  }
  if (this.e2eCtx?.password) {
    Cypress.env(LAST_USER_PASSWORD, this.e2eCtx.password)
  }
})
