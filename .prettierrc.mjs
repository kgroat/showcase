/** @type {import("prettier").Config} */
const config = {
  semi: false,
  singleQuote: true,
  jsxSingleQuote: true,
}

export default config
