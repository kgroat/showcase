import type { User } from '@supabase/supabase-js'

import type { Database } from '~/supabase/types'

export type RoomStore = {
  loading: boolean
  submittingName: boolean
  editingName: boolean
  temporaryName: string
  room: Database['public']['Tables']['planning_poker_room']['Row'] | null
  members: Database['public']['Tables']['planning_poker_room_member']['Row'][]
  usersById: Record<string, User | undefined>
  ownVote: string | null
}