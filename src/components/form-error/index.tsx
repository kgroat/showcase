import { component$, Slot } from '@builder.io/qwik'

import styles from './styles.module.css'

export const FormError = component$(() => {
  return (
    <div class={styles.formError} data-testid='form-error'>
      <Slot />
    </div>
  )
})
