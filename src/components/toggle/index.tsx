import type { Signal } from '@builder.io/qwik'
import { component$, useId, useSignal } from '@builder.io/qwik'

import styles from './styles.module.css'

export type ThemeToggleProps = {
  name: string
  'bind:checked'?: Signal<boolean>
} | {
  name?: string
  'bind:checked': Signal<boolean>
}

export const Toggle = component$<ThemeToggleProps>((props) => {
  const defaultBind = useSignal(false)
  const id = useId()
  const { name, 'bind:checked': bind = defaultBind } = props

  return (
    <>
      <input name={name} class={styles.checkbox} type='checkbox' id={id} bind:checked={bind} />
      <label class={styles.toggle} for={id} />
    </>
  )
})
