import { $ } from '@builder.io/qwik'
import { action } from '@storybook/addon-actions'
import type { Meta, StoryObj } from 'storybook-framework-qwik'

import { FloatingLabelInput, type FloatingLabelInputProps } from './index'

const meta: Meta<FloatingLabelInputProps> = {
  component: FloatingLabelInput,
  args: {
    onChange: $((...args) => action('onChange')(...args)),
  },
}

type Story = StoryObj<FloatingLabelInputProps>

export default meta

export const Default: Story = {
  args: {
    label: 'Email',
    name: 'email',
    type: 'email',
    required: false,
  },
  render: (props) => <FloatingLabelInput {...props} />,
}
