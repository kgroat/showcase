import type { QRL, Signal } from '@builder.io/qwik'
import { component$, Slot, useSignal } from '@builder.io/qwik'

import { FormError } from '../form-error'
import styles from './styles.module.css'

export interface FloatingLabelInputProps {
  label: string
  name: string
  type?: string
  'bind:value'?: Signal<string>
  onChange?: QRL<(value: string) => void>
  errors?: string[]
  required?: boolean
  autoComplete?: string
  'data-testid'?: string
}

export const FloatingLabelInput = component$<FloatingLabelInputProps>((props) => {
  const defaultSignal = useSignal<string>()
  const {
    label,
    name,
    type = 'text',
    ['bind:value']: bindValue = defaultSignal,
    onChange,
    errors,
    required,
    autoComplete = 'off',
    'data-testid': testid,
  } = props

  return (
    <>
      <label class={styles.label}>
        <input
          class={styles.input}
          name={name}
          type={type}
          placeholder=' '
          onChange$={(ev) => onChange?.((ev.target as HTMLInputElement).value)}
          required={required}
          bind:value={bindValue}
          autoComplete={autoComplete}
          data-testid={testid}
        />
        <span class={styles.text}>{label}</span>
        <Slot />
      </label>
      {errors &&
        errors.map((err, idx) => <FormError key={idx}>{err}</FormError>)}
    </>
  )
})
