import { component$ } from '@builder.io/qwik'

import GoogleLogo from '~/media/google.svg?jsx'
import { supabase } from '~/supabase'

import styles from './styles.module.css'

export interface Props {}

export const GoogleButton = component$<Props>(() => {
  return (
    <button
      type='button'
      class={styles.googleButton}
      onClick$={() => {
        supabase.auth.signInWithOAuth({
          provider: 'google',
          options: {
            redirectTo: `${location.protocol}//${location.host}`,
          },
        })
      }}
    >
      <span class={styles.logoContainer}>
        <GoogleLogo class={styles.logo} />
      </span>
      <span class={styles.content}>Log in with Google</span>
    </button>
  )
})
