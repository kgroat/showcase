import { component$, useComputed$, useContext, useSignal, useTask$, useVisibleTask$ } from '@builder.io/qwik'
import { Link } from '@builder.io/qwik-city'

import { themeStateContext } from '~/context/themeState'
import { userStateContext } from '~/context/userState'
import { getUserDisplayName } from '~/helpers/user'
import LogoDark from '~/media/logo-dark.svg?jsx'
import LogoLight from '~/media/logo-light.svg?jsx'

import { Toggle } from '../toggle'
import styles from './header.module.css'

export default component$(() => {
  const userState = useContext(userStateContext)
  const themeState = useContext(themeStateContext)

  const name = useComputed$(() => (
    getUserDisplayName(userState.value)
  ))
  const lightThemeChecked = useSignal(themeState.value === 'light')

  // eslint-disable-next-line qwik/no-use-visible-task
  useVisibleTask$(({ track }) => {
    track(() => themeState.value)
    lightThemeChecked.value = themeState.value === 'light'
  }, { strategy: 'document-idle' })

  useTask$(({ track }) => {
    track(() => lightThemeChecked.value)
    themeState.value = lightThemeChecked.value ? 'light' : 'dark'
  })

  return (
    <header class={styles.header}>
      <div class={['container', styles.wrapper]}>
        <div class={styles.logo}>
          <Link href='/' title='home'>
            {
              themeState.value === 'light'
                ? <LogoDark />
                : <LogoLight />
            }
          </Link>
        </div>

        <ul>
          <li>
            <Toggle bind:checked={lightThemeChecked} />
          </li>
          {userState.value !== null ? (
            <>
              {name.value ? (
                <li data-testid='greeting'>Hello, {name.value}!</li>
              ) : (
                <li data-testid='greeting'>Hello!</li>
              )}
              <li>
                <Link href='/user/me'>My profile</Link>
              </li>
              <li>
                <Link href='/logout'>Log out</Link>
              </li>
              </>
          ) : (
            <>
              <li>
                <Link href='/login'>Login</Link>
              </li>
              <li>
                <Link href='/signup'>Sign up</Link>
              </li>
            </>
          )}
        </ul>
      </div>
    </header>
  )
})
