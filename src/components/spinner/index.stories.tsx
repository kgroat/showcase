import type { Meta, StoryObj } from 'storybook-framework-qwik'

import { Spinner, type SpinnerProps } from './index'

const meta: Meta<SpinnerProps> = {
  component: Spinner,
}

type Story = StoryObj<SpinnerProps>

export default meta

export const Default: Story = {
  args: {
    scale: 1,
    count: 12,
    thickness: 30,
    size: 100,
    color: 'green',
    duration: 1,
  },
  render: (props) => <Spinner {...props} />,
}
