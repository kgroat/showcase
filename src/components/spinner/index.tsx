import { component$ } from '@builder.io/qwik'

import styles from './styles.module.css'

export interface SpinnerProps {
  scale?: number
  count?: number
  thickness?: number
  size?: number
  color?: string
  duration?: number
}

export const Spinner = component$<SpinnerProps>(({
  scale = 1,
  count = 12,
  thickness = 30,
  size = 100,
  color = 'var(--text)',
  duration = 1,
}) => {
  const armSize = size * scale
  const armPosition = (size - armSize) / 2
  const thicknessPixels = armSize * thickness * 30 / 10000
  const top = (armSize - thicknessPixels) / 2
  
  return (
    <div class={styles.spinner} style={{
      position: 'relative',
      width: size,
      height: size,
    }}>
      {Array.from(Array(count)).map((_, i) => (
        <div key={i} style={{
          width: `${100 * scale}%`,
          top: top + armPosition,
          left: armPosition,
          height: thicknessPixels,
          position: 'absolute',
          transform: `rotate(${-360 * i / count - 90}deg)`,
        }}>
          <div style={{
            height: '100%',
            width: '30%',
            position: 'absolute',
            left: '70%',
            background: color,
            borderRadius: armSize,
            animationDelay: `${-duration * i / count}s`,
            animationDuration: `${duration}s`,
          }} />
        </div>
      ))}
    </div>
  )
})
