import { component$ } from '@builder.io/qwik'

import styles from './styles.module.css'

export interface Props {
  value: 0 | 1 | 2 | 3 | 4
}

export const strengthClasses = [
  styles.empty,
  styles.weak,
  styles.medium,
  styles.strong,
  styles.strong,
] as const

export const PasswordStrength = component$<Props>(({ value }) => {
  return (
    <div class={styles.passwordStrength}>
      <div class={styles.gauge}>
        <div class={[styles.fill, strengthClasses[value]]}></div>
      </div>
    </div>
  )
})
