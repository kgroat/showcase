import { component$ } from '@builder.io/qwik'

import GitLabLogo from '~/media/gitlab-logo-500.svg?jsx'

import styles from './footer.module.css'

export default component$(() => {
  return (
    <footer>
      <div class='container'>
        <a href='https://www.kgroat.dev/' target='_blank' class={styles.anchor}>
          <span>Made with ♡ by Kevin Groat</span>
        </a>
      </div>
      <a
        class={styles.gitlabLink}
        href='https://gitlab.com/kgroat-planning-poker/showcase'
        target='_blank'
      >
        <GitLabLogo class={styles.gitlabLogo} />
      </a>
    </footer>
  )
})
