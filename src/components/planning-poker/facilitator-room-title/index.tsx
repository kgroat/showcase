import { component$ } from '@builder.io/qwik'

import CancelIcon from '~/media/icons/close.svg?jsx'
import DoneIcon from '~/media/icons/done.svg?jsx'
import EditIcon from '~/media/icons/edit.svg?jsx'
import { supabase } from '~/supabase'
import type { RoomStore } from '~/types/planning-poker'

import styles from './styles.module.css'

export interface Props {
  roomStore: RoomStore
}

export const FacilitatorRoomTitle = component$<Props>(({ roomStore }) => {
  return (roomStore.editingName
    ? (
      <form
        class={styles.updateForm}
        preventdefault:submit
        preventdefault:reset
        onSubmit$={async () => {
          roomStore.submittingName = true
          try {
            const resp = await supabase
              .from('planning_poker_room')
              .update({ name: roomStore.temporaryName })
              .eq('id', roomStore.room!.id!)

            if (!resp.error) {
              roomStore.temporaryName = ''
              roomStore.editingName = false
            }
          } finally {
            roomStore.submittingName = false
          }
        }}
        onReset$={() => {
          roomStore.temporaryName = ''
          roomStore.editingName = false
        }}
      >
        <input
          class='h1-input'
          style={{ width: '50vw' }}
          value={roomStore.temporaryName}
          onChange$={(ev) => roomStore.temporaryName = (ev.target as HTMLInputElement).value}
        />
        <span class={styles.buttons}>
          <button
            type='reset'
            class={styles.iconButton}
            disabled={roomStore.submittingName}
          ><CancelIcon /></button>
          <button
            type='submit'
            class={styles.iconButton}
            disabled={roomStore.submittingName}
          ><DoneIcon /></button>
        </span>
      </form>
    )
    : ((
      <>
        <span>{roomStore.room?.name}</span>
        <button 
          type='button'
          class={styles.iconButton}
          onClick$={() => {
            roomStore.temporaryName = roomStore.room?.name ?? ''
            roomStore.editingName = true
          }}
        ><EditIcon /></button>
      </>
    )
  )
  )
})
