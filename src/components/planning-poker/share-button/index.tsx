import { component$, useSignal } from '@builder.io/qwik'

import CopyIcon from '~/media/icons/content-copy.svg?jsx'
import DoneIcon from '~/media/icons/done.svg?jsx'

interface Props {
  roomId: string
}

export const ShareButton = component$<Props>(({ roomId }) => {
  const shareCopied = useSignal(false)
  const shareUrl = `${location.protocol}//${location.host}/planning-poker/join/${roomId}`

  return (
    <button
      type='button'
      class='button-with-icon'
      onClick$={async () => {
        await navigator.clipboard.writeText(shareUrl)
        shareCopied.value = true
        setTimeout(() => {
          shareCopied.value = false
        }, 1000)
      }}
    >
      {
        shareCopied.value
          ? <>Copied! <DoneIcon /></>
          : <>Share <CopyIcon /></>
      }
    </button>
  )
})
