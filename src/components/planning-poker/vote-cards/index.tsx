import { component$, type ReadonlySignal } from '@builder.io/qwik'
import type { User } from '@supabase/supabase-js'

import { setOwnVote } from '~/apis/planning-poker'
import { VoteCard } from '~/components/planning-poker/vote-card'
import type { RoomStore } from '~/types/planning-poker'

interface Props {
  roomStore: RoomStore
  user: ReadonlySignal<User | null>
}

export const VOTE_OPTIONS = [
  '0.5',
  '1',
  '2',
  '3',
  '5',
  '8',
  '13',
  '21',
  'infinity',
  'coffee',
] as const

export const VoteCards = component$<Props>(({ roomStore, user }) => {
  return (
    <>
      {
        VOTE_OPTIONS.map((option) => (
          <VoteCard
            key={option}
            value={option}
            highlight={roomStore.ownVote === option}
            onClick$={(val: string) => {
              roomStore.ownVote = val
              setOwnVote(roomStore.room!.id, user.value!.id, val)
            }}
          />
        ))
      }
    </>
  )
})
