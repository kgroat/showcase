import { component$, type ReadonlySignal } from '@builder.io/qwik'

import { clearVotes, toggleShowVotes } from '~/apis/planning-poker'
import ClearIcon from '~/media/icons/cancel.svg?jsx'
import ShowIcon from '~/media/icons/visibility.svg?jsx'
import HideIcon from '~/media/icons/visibility-off.svg?jsx'
import type { RoomStore } from '~/types/planning-poker'

import { ShareButton } from '../share-button'

interface Props {
  userIsFacilitator: ReadonlySignal<boolean>
  roomStore: RoomStore
}

export const RoomControls = component$<Props>(({ userIsFacilitator, roomStore }) => {
  const room = roomStore.room!

  return (
    userIsFacilitator.value ? (
      <>
        <button
          type='button'
          class='button-with-icon'
          onClick$={() => toggleShowVotes(room.id, !room.votes_visible)}
        >
          {
            room.votes_visible
              ? <>Hide votes <HideIcon  /></>
              : <>Show votes <ShowIcon  /></>
          }
        </button>
        <button
          type='button'
          class='button-with-icon'
          onClick$={() => clearVotes(room.id)}
        >
          Clear votes
          <ClearIcon />
        </button>
        <ShareButton roomId={room.id} />
      </>
    ) : (
      <ShareButton roomId={room.id} />
    )
  )
})
