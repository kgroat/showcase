import { component$, type QRL } from '@builder.io/qwik'

import Infinity from '~/media/icons/all-inclusive.svg?jsx'
import Coffee from '~/media/icons/local-cafe.svg?jsx'

import styles from './styles.module.css'

interface Props {
  value: string
  highlight?: boolean
  onClick$?: QRL<(value: string) => void>
}

export const VoteCard = component$<Props>(({ value, onClick$, highlight }) => {
  let voteContent = <>{value}</>
  if (value === 'infinity') {
    voteContent = <Infinity />
  } else if (value === 'coffee') {
    voteContent = <Coffee />
  } else if (value === '0.5') {
    voteContent = <>½</>
  }

  if (onClick$) {
    return (
      <button
        type='button'
        class={{
          [styles.card]: true,
          [styles.clickable]: true,
          [styles.highlight]: highlight,
        }}
        onClick$={() => onClick$(value)}
      >
        {voteContent}
      </button>
    )
  } else {
    return (
      <div class={{
        [styles.card]: true,
        [styles.highlight]: highlight,
      }}>
        {voteContent}
      </div>
    )
  }
})
