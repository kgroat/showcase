import { component$, type ReadonlySignal } from '@builder.io/qwik'
import type { User } from '@supabase/supabase-js'

import { UserCard } from '~/components/planning-poker/user-card'
import type { RoomStore } from '~/types/planning-poker'

interface Props {
  roomStore: RoomStore
  user: ReadonlySignal<User | null>
}

export const MemberUserCards = component$<Props>(({ roomStore, user }) => {
  return (
    <>
      {
        roomStore.members.map(m => {
          const memberUser = roomStore.usersById[m.user_id] ?? null
          return (
            <UserCard
              key={m.id}
              room={roomStore.room}
              member={m}
              user={memberUser}
              isFacilitator={roomStore.room?.facilitator_id === m.user_id}
              isYou={user.value?.id === m.user_id}
            />
          )
        })
      }
    </>
  )
})
