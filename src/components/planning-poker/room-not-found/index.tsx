import { component$ } from '@builder.io/qwik'
import { Link } from '@builder.io/qwik-city'

export const RoomNotFound = component$(() => {
  return (
    <div class='container container-center'>
      <h1><span class='highlight'>404</span> Room Not Found</h1>
      <h2>The room you are trying to join does not exist.</h2>
      <p><Link href='/planning-poker/create'>Create a new room instead</Link></p>
    </div>
  )
})
