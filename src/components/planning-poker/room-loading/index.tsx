import { component$ } from '@builder.io/qwik'

export const RoomLoading = component$(() => {
  return (
    <div class='container container-center'>
      <h1><span class='highlight'>Loading</span>...</h1>
    </div>
  )
})
