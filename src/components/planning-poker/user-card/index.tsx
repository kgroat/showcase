import { component$ } from '@builder.io/qwik'
import { Link } from '@builder.io/qwik-city'
import type { User } from '@supabase/supabase-js'

import { getUserDisplayNameWithBreaks } from '~/helpers/user'
import type { Database } from '~/supabase/types'

import { VoteCard } from '../vote-card'
import styles from './styles.module.css'

interface Props {
  room: Database['public']['Tables']['planning_poker_room']['Row'] | null
  member: Database['public']['Tables']['planning_poker_room_member']['Row']
  user: User | null
  isFacilitator?: boolean
  isYou?: boolean
}

export const UserCard = component$<Props>(({ room, member, user, isFacilitator, isYou }) => {
  const hasVote = !!member.vote
  return (
    <div class={styles.userCard}>
      <VoteCard
        value={
          room?.votes_visible
            ? member.vote || '…'
            : hasVote ? '✅' : '🤔'
        }
      />
      <div>
        <Link href={`/user/${member.user_id}`}>
          {getUserDisplayNameWithBreaks(user) ?? member.user_id}
        </Link>
        {
          isFacilitator
            ? <><br />(Facilitator)</>
            : isYou && <><br />(You)</>
        }
      </div>
    </div>
  )
})
