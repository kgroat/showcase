import type { User } from '@supabase/supabase-js'
import type { ColumnType, Generated, JSONColumnType } from 'kysely'

import type { getUserById } from './index'

export type UserMetadata = {
  name?: string
  picture?: string
}

export type UsersTable = Pick<User, 'email' | 'last_sign_in_at'> & {
  /* COMMON */
  id: Generated<string>
  created_at: ColumnType<Date, never, never>

  /* CUSTOM */
  raw_user_meta_data: JSONColumnType<UserMetadata>
}

export type UsersField = keyof UsersTable

export type PublicUser = NonNullable<Awaited<ReturnType<typeof getUserById>>>
