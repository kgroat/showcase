import type { DB, Selections } from '../db'

export const publicUsersFields = [
  'id',
  'email',
  'raw_user_meta_data as metadata',
] as const satisfies Selections<'auth.users'>

export async function getUserById(db: DB, id: string) {
  const user = await db
    .selectFrom('auth.users')
    .innerJoin(
      'planning_poker_room as foo',
      'foo.facilitator_id',
      'auth.users.id',
    )
    .select(publicUsersFields)
    .where('id', '=', id)
    .executeTakeFirst()

  return user
}
