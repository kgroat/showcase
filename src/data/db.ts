import type { EnvGetter } from '@builder.io/qwik-city/middleware/request-handler'
import { readFileSync } from 'fs'
import type { SelectExpression } from 'kysely'
import { Kysely, PostgresDialect } from 'kysely'
import pg from 'pg'

import type { PlanningPokerRoomTable } from './planningPokerRoom/types'
import type { PlanningPokerRoomMemberTable } from './planningPokerRoomMember/types'
import type { UsersTable } from './users/types'

export interface Database {
  'auth.users': UsersTable
  planning_poker_room: PlanningPokerRoomTable
  planning_poker_room_member: PlanningPokerRoomMemberTable
}

export type Selections<
  TKey extends keyof TDatabase,
  TDatabase extends Database = Database,
> = readonly SelectExpression<TDatabase, TKey>[]
export type ExtendedDatabase<
  TSource extends keyof TDatabase,
  TDest extends string,
  TDatabase extends Database = Database,
> = Database & { [dest in TDest]: TDatabase[TSource] }

export type DB = Kysely<Database>
export let cachedDb: DB | null = null

export const getDb = (env: EnvGetter) => {
  if (!cachedDb) {
    const caCert = env.get('POSTGRES_SSL')
    const dialect = new PostgresDialect({
      pool: new pg.Pool({
        connectionString: env.get('POSTGRES_CONNECTION_STRING'),
        port: 5432,
        max: 10,
        ssl: caCert
          ? {
              ca: readFileSync(caCert),
            }
          : undefined,
      }),
    })

    cachedDb = new Kysely<Database>({
      dialect,
    })
  }

  return cachedDb
}
