import type { Generated } from 'kysely'

// import type { getPlanningPokerRoomById } from './index'

export type PlanningPokerRoomMemberTable = {
  /* COMMON */
  id: Generated<string>
  // created_at: ColumnType<Date, never, never>
  // modified_at: ColumnType<Date, never, string>

  /* CUSTOM */
  room_id: string
  user_id: string
  vote: string | null
}

export type PlanningPokerRoomMemberField = keyof PlanningPokerRoomMemberTable

// export type PublicPlanningPokerRoom = NonNullable<Awaited<ReturnType<typeof getPlanningPokerRoomById>>>
