import type { DB, Selections } from '../db'

export const publicMemberFields = [
  'id',
  'room_id',
  'vote',
  'user_id',
] as const satisfies Selections<'planning_poker_room_member'>

export async function getMembersByRoomId(db: DB, roomId: string) {
  const members = await db
    .selectFrom('planning_poker_room_member as member')
    .innerJoin('auth.users as user', 'member.user_id', 'user.id')
    .select(publicMemberFields)
    .where('member.room_id', '=', roomId)
    .execute()

  return members
}

export async function getMemberByRoomAndUser(db: DB, userId: string, roomId: string) {
  const member = await db
    .selectFrom('planning_poker_room_member')
    .where('user_id', '=', userId)
    .where('room_id', '=', roomId)
    .select(publicMemberFields)
    .executeTakeFirst()

  return member
}

export async function addUserToRoom(db: DB, userId: string, roomId: string) {
  const member = await db
    .insertInto('planning_poker_room_member')
    .values({
      room_id: roomId,
      user_id: userId,
    })
    .executeTakeFirstOrThrow()

  return member
}
