import type { User } from '@supabase/supabase-js'

import type { DB, Selections } from '../db'

export const publicPlanningPokerRoomFields = [
  'id',
  'name',
  'facilitator_id',
  'votes_visible',
] as const satisfies Selections<'planning_poker_room'>

export async function createPlanningPokerRoom(
  db: DB,
  name: string,
  facilitator: User,
) {
  const room = await db
    .insertInto('planning_poker_room')
    .values({
      name,
      facilitator_id: facilitator.id,
    })
    .returning(publicPlanningPokerRoomFields)
    .executeTakeFirstOrThrow()

  return room
}

export async function getPlanningPokerRoomById(db: DB, id: string) {
  const room = await db
    .selectFrom('planning_poker_room')
    .where('id', '=', id)
    .select(publicPlanningPokerRoomFields)
    .executeTakeFirst()

  return room ?? null
}
