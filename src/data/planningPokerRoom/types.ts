import type { ColumnType, Generated } from 'kysely'

import type { getPlanningPokerRoomById } from './index'

export type PlanningPokerRoomTable = {
  /* COMMON */
  id: Generated<string>
  // created_at: ColumnType<Date, never, never>
  // modified_at: ColumnType<Date, never, string>

  /* CUSTOM */
  name: string
  facilitator_id: string
  votes_visible: ColumnType<boolean, null, boolean>
}

export type PlanningPokerRoomField = keyof PlanningPokerRoomTable

export type PublicPlanningPokerRoom = NonNullable<
  Awaited<ReturnType<typeof getPlanningPokerRoomById>>
>
