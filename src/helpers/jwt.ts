import type { SupabaseClient } from '@supabase/supabase-js'

export async function verifyJwt(supabase: SupabaseClient, jwt: string) {
  const user = await supabase.auth.getUser(jwt)
  return user
}
