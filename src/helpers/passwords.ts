import { passwordStrength } from 'check-password-strength'

export const PASSPHRASE_LENGTH = 20
export function getPasswordStrength(password: string): 0 | 1 | 2 | 3 | 4 {
  if (password.length >= PASSPHRASE_LENGTH) {
    return 4
  }

  return passwordStrength(password).id as 0 | 1 | 2 | 3
}
