import type { Database, Selections } from '~/data/db'

export type AsSelections<
  TKey extends keyof Database,
  TPrefix extends string,
  TSel extends Selections<TKey>,
> = TSel extends readonly [
  infer Sel1 extends string,
  ...infer Rest extends any[],
]
  ? Sel1 extends `${infer TOrig} as ${infer TRename}`
    ? readonly [
        `${TPrefix}.${TOrig} as ${TRename}`,
        ...PrefixedSelections<TKey, TPrefix, Rest>,
      ]
    : readonly [
        `${TPrefix}.${Sel1} as ${Sel1}`,
        ...PrefixedSelections<TKey, TPrefix, Rest>,
      ]
  : readonly []

export type PrefixedSelections<
  TKey extends keyof Database,
  TPrefix extends string,
  TSel extends Selections<TKey>,
> = TSel extends readonly [
  infer Sel1 extends string,
  ...infer Rest extends any[],
]
  ? Sel1 extends `${infer TOrig} as ${infer TRename}`
    ? readonly [
        `${TPrefix}.${TOrig} as ${TPrefix}_${TRename}`,
        ...PrefixedSelections<TKey, TPrefix, Rest>,
      ]
    : readonly [
        `${TPrefix}.${Sel1} as ${TPrefix}_${Sel1}`,
        ...PrefixedSelections<TKey, TPrefix, Rest>,
      ]
  : readonly []

export function asSelections<
  TKey extends keyof Database,
  TPrefix extends string,
  TSel extends Selections<TKey>,
>(rename: `${TKey} as ${TPrefix}`, selections: TSel) {
  const prefix = rename.split(' as ')[1]
  return selections.map((sel) => {
    if (typeof sel === 'string') {
      if (sel.includes(' as ')) {
        const [origName, rename] = sel.split(' as ')
        return `${prefix}.${origName} as ${rename}`
      } else {
        return `${prefix}.${sel} as ${sel}`
      }
    } else {
      return sel
    }
  }) as readonly string[] as AsSelections<TKey, TPrefix, TSel>
}

export function prefixSelections<
  TKey extends keyof Database,
  TPrefix extends string,
  TSel extends Selections<TKey>,
>(rename: `${TKey} as ${TPrefix}`, selections: TSel) {
  const prefix = rename.split(' as ')[1]
  return selections.map((sel) => {
    if (typeof sel === 'string') {
      if (sel.includes(' as ')) {
        const [origName, rename] = sel.split(' as ')
        return `${prefix}.${origName} as ${prefix}_${rename}`
      } else {
        return `${prefix}.${sel} as ${prefix}_${sel}`
      }
    } else {
      return sel
    }
  }) as readonly string[] as PrefixedSelections<TKey, TPrefix, TSel>
}
