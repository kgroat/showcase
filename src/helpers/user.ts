import type{ User } from '@supabase/supabase-js'

export function getUserDisplayName (user: User | null): string | null {
  if (!user) {
    return null
  }

  // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
  if (user.user_metadata?.name) {
    return user.user_metadata.name
  }

  if (user.email) {
    return user.email
  }

  return user.id
}

export function getUserDisplayNameWithBreaks (user: User | null): string | null {
  const name = getUserDisplayName(user)
  if (!name) return null

  return name.replace(/@|\+|\./g, (char) => `\u200B${char}\u200B`)
}
