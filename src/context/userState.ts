import type { ReadonlySignal } from '@builder.io/qwik'
import { createContextId } from '@builder.io/qwik'
import type { User } from '@supabase/supabase-js'

export const userStateContext =
  createContextId<ReadonlySignal<User | null>>('app.supabase.user')
