import type { Signal } from '@builder.io/qwik'
import { createContextId } from '@builder.io/qwik'

export type Theme = 'light' | 'dark'

export const themeStateContext =
  createContextId<Signal<Theme>>('app.theme')
