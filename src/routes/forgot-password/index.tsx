import { component$, useSignal, useStylesScoped$ } from '@builder.io/qwik'
import type { DocumentHead } from '@builder.io/qwik-city'

import { FloatingLabelInput } from '~/components/floating-label-input'
import { Spinner } from '~/components/spinner'
import { supabase } from '~/supabase'

import styles from './forgot-password.css?inline'

export default component$(() => {
  useStylesScoped$(styles)
  const emailSignal = useSignal('')
  const loading = useSignal(false)
  const submitted = useSignal(false)

  if (submitted.value) {
    return (
      <div class='content'>
        If you have an account, an email has been sent to <span class='highlight'>{emailSignal.value}</span> with
        a link that will allow you to change your password.
      </div>
    )
  }

  if (loading.value) {
    return (
      <div class='column'>
        <Spinner />
      </div>
    )
  }

  return (
    <form
      class='content column'
      preventdefault:submit
      onSubmit$={async () => {
        loading.value = true
        try {
          await supabase.auth.resetPasswordForEmail(emailSignal.value, {
            redirectTo: `${location.protocol}//${location.host}/reset-password`,
          })
          submitted.value = true
        } finally {
          loading.value = false
        }
      }}
    >
      <div>Request an email that will allow you to reset your password</div>
      <FloatingLabelInput
        label='Email'
        name='email'
        bind:value={emailSignal}
      />
      <button type='submit'>Request</button>
    </form>
  )
})

export const head: DocumentHead = {
  title: 'Forgot password',
}
