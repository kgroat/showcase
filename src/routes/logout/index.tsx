import { component$, useStylesScoped$, useVisibleTask$ } from '@builder.io/qwik'
import type { DocumentHead } from '@builder.io/qwik-city'
import { useNavigate } from '@builder.io/qwik-city'

import { supabase } from '~/supabase'

import styles from './logout.css?inline'

export default component$(() => {
  useStylesScoped$(styles)
  const navigate = useNavigate()

  // eslint-disable-next-line qwik/no-use-visible-task
  useVisibleTask$(
    async () => {
      await supabase.auth.signOut()
      navigate('/', { replaceState: true })
    },
    {
      strategy: 'document-ready',
    },
  )

  return (
    <div class='container container-center'>
      <h1>Logging you out...</h1>
    </div>
  )
})

export const head: DocumentHead = {
  title: 'Logging you out...',
}
