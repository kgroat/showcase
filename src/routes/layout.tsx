import {
  component$,
  Slot,
  useContextProvider,
  useSignal,
  useStyles$,
  useTask$,
} from '@builder.io/qwik'
import { type RequestHandler, routeLoader$ } from '@builder.io/qwik-city'
import type { User } from '@supabase/supabase-js'

import Footer from '~/components/footer/footer'
import Header from '~/components/header/header'
import { AUTH_COOKIE } from '~/constants/cookies'
import { type Theme, themeStateContext } from '~/context/themeState'
import { userStateContext } from '~/context/userState'
import { getAdminSupabase, supabase } from '~/supabase'

import styles from './styles.css?inline'

export const USER_THEME_KEY = 'user-theme'

export const onGet: RequestHandler = async ({ cacheControl }) => {
  // Control caching for this request for best performance and to reduce hosting costs:
  // https://qwik.builder.io/docs/caching/
  cacheControl({
    // Always serve a cached response by default, up to a week stale
    staleWhileRevalidate: 60 * 60 * 24 * 7,
    // Max once every 5 seconds, revalidate on the server to get a fresh version of this page
    maxAge: 0,
  })
}

export const useUserTheme = routeLoader$(({ cookie }): Theme => {
  const theme = cookie.get(USER_THEME_KEY)?.value

  if (theme === 'light' || theme === 'dark') {
    return theme
  } else {
    return 'dark'
  }
})

export const useLoggedInUser = routeLoader$(async ({ cookie, env }) => {
  const jwt = cookie.get(AUTH_COOKIE)?.value
  if (!jwt) {
    return null
  }

  const supabase = getAdminSupabase(env)
  const response = await supabase.auth.getUser(jwt)

  if (response.error) {
    console.warn('JWT error', response.error)
    return null
  } else {
    return response.data.user
  }
})

export default component$(() => {
  const loggedInUser = useLoggedInUser()
  const userState = useSignal<User | null>(loggedInUser.value)
  useStyles$(styles)
  
  const themeState = useSignal<Theme>(useUserTheme().value)
  useContextProvider(themeStateContext, themeState)

  useTask$(({ track }) => {
    track(() => themeState.value)
    if (typeof document !== 'undefined') document.cookie = `${USER_THEME_KEY}=${themeState.value}; path=/;max-age=${365 * 24 * 60 * 60}`
  })


  useTask$(
    () => {
      function setAuthCookie(value: string) {
        if (typeof document === 'undefined') return
        document.cookie = `${AUTH_COOKIE}=${value}; path=/`
      }

      function clearAuthCookie() {
        if (typeof document === 'undefined') return
        document.cookie = `${AUTH_COOKIE}=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT`
      }

      const {
        data: { subscription },
      } = supabase.auth.onAuthStateChange((ev, session) => {
        switch (ev) {
          case 'INITIAL_SESSION':
            userState.value = session?.user ?? null
            if (session?.access_token) {
              setAuthCookie(session.access_token)
            } else {
              clearAuthCookie()
            }
            return
          case 'SIGNED_IN':
          case 'TOKEN_REFRESHED':
          case 'USER_UPDATED':
            if (!session) {
              console.warn('User logged in without a session')
              return
            }

            userState.value = session.user
            setAuthCookie(session.access_token)
            return
          case 'SIGNED_OUT':
            userState.value = null
            clearAuthCookie()
            return
        }
      })

      return () => {
        subscription.unsubscribe()
      }
    },
  )

  useContextProvider(userStateContext, userState)

  return (
    <div
      id='app'
      class={{
        'theme-light': themeState.value === 'light',
        'theme-dark': themeState.value === 'dark',
      }}
    >
      <Header />
      <main>
        <Slot />
      </main>
      <Footer />
    </div>
  )
})
