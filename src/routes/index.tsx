import { component$, useContext } from '@builder.io/qwik'
import { type DocumentHead, Link } from '@builder.io/qwik-city'

import { userStateContext } from '~/context/userState'

export default component$(() => {
  const userState = useContext(userStateContext)

  return (
    <div class='container container-center container-gap' data-testid='home-page'>
      <h1>Welcome to my planning poker app!</h1>
      <h2>
        {
          userState.value
            ? <Link href='/planning-poker/create'>Check out Planning Poker</Link>
            : <Link href='/login'>Log in to continue</Link>
        }
      </h2>
    </div>
  )
})

export const head: DocumentHead = {
  title: 'Welcome to my planning poker app!',
  // meta: [
  //   {
  //     name: 'description',
  //     content: 'Qwik site description',
  //   },
  // ],
}
