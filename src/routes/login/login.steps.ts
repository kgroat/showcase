import { Given, Then, When } from '@badeball/cypress-cucumber-preprocessor'

import { emailNonce } from '~/test-utils/email'

Given('I am on the login page', function () {
  cy.visit('/login')
})

When('I enter my login details', function () {
  cy.findByLabelText('Email').type(this.e2eCtx!.email!)
  cy.findByLabelText('Password').type(this.e2eCtx!.password!)

  cy.get('form').findByText('Login').click()
})

When('I enter incorrect login details', function () {
  cy.findByLabelText('Email').type(`foo+${emailNonce()}@bar.com`)
  cy.findByLabelText('Password').type('Incorrect password')

  cy.get('form').findByText('Login').click()
})

Then('I see an invalid credentials error', function () {
  cy.findAllByTestId('form-error')
    .contains('Invalid login credentials')
    .should('be.visible')
})
