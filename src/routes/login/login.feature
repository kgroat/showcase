Feature: Login page

  Scenario: As a user, I can log in
    Given I have a user account
    And I am on the login page
    When I enter my login details
    Then I am on the home page
    And I should be logged in

  Scenario: As a user, I can't log in with the wrong credentials
    Given I am on the login page
    When I enter incorrect login details
    Then I see an invalid credentials error
