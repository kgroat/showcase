import { $, component$, useSignal, useStylesScoped$ } from '@builder.io/qwik'
import type { DocumentHead } from '@builder.io/qwik-city'
import { Link, useNavigate } from '@builder.io/qwik-city'

import { FloatingLabelInput } from '~/components/floating-label-input'
import { FormError } from '~/components/form-error'
import { GoogleButton } from '~/components/google-button'
import { supabase } from '~/supabase'

import styles from './login.css?inline'

const emailKey = 'email'
const passwordKey = 'password'

export default component$(() => {
  useStylesScoped$(styles)
  const navigate = useNavigate()
  const formErrors = useSignal<string[]>([])
  const submitLogin = $(async ({ formData }: { formData: FormData }) => {
    const email = formData.get(emailKey) as string
    const password = formData.get(passwordKey) as string

    const resp = await supabase.auth.signInWithPassword({
      email,
      password,
    })

    if (resp.error) {
      formErrors.value = [resp.error.message]
    } else {
      navigate(sessionStorage.getItem('returnTo') ?? '/')
      sessionStorage.removeItem('returnTo')
    }
  })

  return (
    <div class='container container-center column'>
      <form
        class='column'
        preventdefault:submit
        onSubmit$={(_ev, form) => {
          const formData = new FormData(form)
          submitLogin({ formData })
        }}
      >
        <FloatingLabelInput
          label='Email'
          type='email'
          name={emailKey}
          autoComplete='email'
          required
        />
        <FloatingLabelInput
          label='Password'
          type='password'
          name={passwordKey}
          autoComplete='current-password'
          required
        />
        {formErrors.value.map((err, idx) => (
          <FormError key={idx}>{err}</FormError>
        ))}
        <Link href='/forgot-password'>Forgot password?</Link>
        <button type='submit'>Login</button>
      </form>
      <GoogleButton />
    </div>
  )
})

export const head: DocumentHead = {
  title: 'Log in',
}
