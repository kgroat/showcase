import { Then } from '@badeball/cypress-cucumber-preprocessor'

Then('I am on the home page', function () {
  cy.location().should('have.a.property', 'pathname', '/')

  cy.findByTestId('home-page').should('be.visible')
})
