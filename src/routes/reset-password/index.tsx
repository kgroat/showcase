import { $, component$, useComputed$, useSignal, useStylesScoped$ } from '@builder.io/qwik'
import { type DocumentHead, Link,z } from '@builder.io/qwik-city'

import { FloatingLabelInput } from '~/components/floating-label-input'
import { FormError } from '~/components/form-error'
import { PasswordStrength } from '~/components/password-strength'
import { Spinner } from '~/components/spinner'
import { getPasswordStrength } from '~/helpers/passwords'
import { supabase } from '~/supabase'

import styles from './reset-password.css?inline'

const passwordKey = 'password'
const verifyPasswordKey = 'verifyPassword'

export const stateChecker = z
  .object({
    password: z
      .string({
        required_error: 'Please enter a password.',
      })
      .refine((pass) => getPasswordStrength(pass) >= 2, {
        message:
          'Your password is too weak.  Try using a longer password with upper and lowercase letters, numbers, and symbols',
      }),
      verifyPassword: z.string(),
  })
  .refine(({ password, verifyPassword }) => password === verifyPassword, {
    message: 'Passwords must match.',
  })

export default component$(() => {
  useStylesScoped$(styles)
  const loading = useSignal(false)
  const submitted = useSignal(false)
  const passwordErrors = useSignal<string[]>([])
  const verifyPasswordErrors = useSignal<string[]>([])
  const formErrors = useSignal<string[]>([])

  const passwordSignal = useSignal('')
  const passwordStrength = useComputed$(() => {
    return getPasswordStrength(passwordSignal.value)
  })

  const changePassword = $(async ({ formData }: { formData: FormData }) => {
    const password = formData.get(passwordKey) as string
    const verifyPassword = formData.get(verifyPasswordKey) as string

    const result = stateChecker.safeParse({
      password,
      verifyPassword,
    })

    if (!result.success) {
      passwordErrors.value = result.error.formErrors.fieldErrors.password ?? []
      verifyPasswordErrors.value =
        result.error.formErrors.fieldErrors.verifyPassword ?? []
      formErrors.value = result.error.formErrors.formErrors
      return
    }

    // Clear all errors
    passwordErrors.value = []
    verifyPasswordErrors.value = []
    formErrors.value = []

    loading.value = true
    try {
      const resp = await supabase.auth.updateUser({
        password,
      })
  
      if (resp.error) {
        formErrors.value = [resp.error.message]
      } else {
        submitted.value = true
      }
    } finally {
      loading.value = false
    }
  })

  if (submitted.value) {
    return (
      <div class='content column'>
        Your password has been successfully reset.
        <Link href='/'>Go Home</Link>
      </div>
    )
  }

  if (loading.value) {
    return (
      <div class='column'>
        <Spinner />
      </div>
    )
  }


  return (
    <form
      class='content column'
      preventdefault:submit
      onSubmit$={async (_ev, form) => {
        await changePassword({ formData: new FormData(form) })
      }}
    >
      <FloatingLabelInput
        label='New Password'
        type='password'
        name={passwordKey}
        autoComplete='new-password'
        required
        errors={passwordErrors.value}
        bind:value={passwordSignal}
      >
        <PasswordStrength value={passwordStrength.value} />
      </FloatingLabelInput>
      <FloatingLabelInput
        label='Verify Password'
        type='password'
        name={verifyPasswordKey}
        autoComplete='new-password'
        required
        errors={verifyPasswordErrors.value}
      />
      {formErrors.value.map((err, idx) => (
        <FormError key={idx}>{err}</FormError>
      ))}
      <button type='submit'>Submit</button>
    </form>
  )
})

export const head: DocumentHead = {
  title: 'Reset password',
}
