import {
  component$,
  useComputed$,
  useContext,
  useStore,
  useStylesScoped$,
  useVisibleTask$,
} from '@builder.io/qwik'
import type { DocumentHead } from '@builder.io/qwik-city'
import { useLocation, useNavigate } from '@builder.io/qwik-city'

import { FacilitatorRoomTitle } from '~/components/planning-poker/facilitator-room-title'
import { MemberUserCards } from '~/components/planning-poker/member-user-cards'
import { RoomControls } from '~/components/planning-poker/room-controls'
import { RoomLoading } from '~/components/planning-poker/room-loading'
import { RoomNotFound } from '~/components/planning-poker/room-not-found'
import { VoteCards } from '~/components/planning-poker/vote-cards'
import { userStateContext } from '~/context/userState'
import { supabase } from '~/supabase'
import type { Database } from '~/supabase/types'
import type { RoomStore } from '~/types/planning-poker'

import styles from './room.css?inline'

export default component$(() => {
  useStylesScoped$(styles)

  const navigate = useNavigate()
  const user = useContext(userStateContext)
  const loc = useLocation()
  const roomStore = useStore<RoomStore>({
    loading: true,
    submittingName: false,
    editingName: false,
    temporaryName: '',
    room: null,
    members: [],
    usersById: {},
    ownVote: null,
  })

  const userIsFacilitator = useComputed$(() => {
    if (!user.value) {
      return false
    }
    return roomStore.room?.facilitator_id === user.value.id
  })

  // eslint-disable-next-line qwik/no-use-visible-task
  useVisibleTask$(async () => {
    if (!user.value) {
      navigate(`/planning-poker/join/${loc.params.roomId}`)
      return
    }
    async function fetchUser ({ user_id }: Database['public']['Tables']['planning_poker_room_member']['Row']) {
      const response = await fetch(`/user/${user_id}`, {
        headers: {
          Accept: 'application/json',
        },
      })
      if (response.ok) {
        roomStore.usersById[user_id] = await response.json()
      }
      return roomStore.usersById[user_id]
    }

    try {
      const [
        { data: room, error: roomError },
        { data: members, error: membersError },
      ] = await Promise.all([
        supabase
          .from('planning_poker_room')
          .select()
          .eq('id', loc.params.roomId)
          .limit(1)
          .single(),
        supabase
          .from('planning_poker_room_member')
          .select()
          .eq('room_id', loc.params.roomId),
      ])
      
      if (roomError || membersError) {
        navigate(`/planning-poker/join/${loc.params.roomId}`)
        return
      }
  
      roomStore.room = room
      roomStore.members = members ?? []

      const ownMember = members?.find(m => m.user_id === user.value?.id)
      if (ownMember) {
        roomStore.ownVote = ownMember.vote
      }

      members?.forEach(fetchUser)
    } finally {
      roomStore.loading = false
    }

    const supaChannel = supabase
      .channel('any')
      .on<Database['public']['Tables']['planning_poker_room']['Row']>(
        'postgres_changes',
        { event: 'UPDATE', schema: 'public', table: 'planning_poker_room', filter: `id=eq.${loc.params.roomId}` },
        (payload) => {
          console.log('Room change!', payload)

          if (roomStore.room) {
            roomStore.room.name = payload.new.name
            roomStore.room.votes_visible = payload.new.votes_visible
          }
        },
      )
      .on<Database['public']['Tables']['planning_poker_room_member']['Row']>(
        'postgres_changes',
        { event: '*', schema: 'public', table: 'planning_poker_room_member', filter: `room_id=eq.${loc.params.roomId}` },
        (payload) => {
          console.log('Member change!', payload)

          switch (payload.eventType) {
            case 'INSERT': {
              roomStore.members.push(payload.new)
              fetchUser(payload.new)
              return
            }
            case 'UPDATE': {
              const member = roomStore.members.find(m => m.id === payload.new.id)
              if (!member) return
              member.vote = payload.new.vote
              if (member.user_id === user.value?.id && payload.new.vote === null) {
                roomStore.ownVote = null
              }
              return
            }
            case 'DELETE': {
              const idx = roomStore.members.findIndex(m => m.id === payload.old.user_id)
              if (idx >= 0) {
                roomStore.members = [...roomStore.members.slice(0, idx), ...roomStore.members.slice(idx + 1)]
              }
              return
            }
          }
        },
      )
      .subscribe()

    return () => {
      supaChannel.unsubscribe()
    }
  }, { strategy: 'document-ready' })

  if (roomStore.loading) {
    return <RoomLoading />
  }

  if (!roomStore.room) {
    return <RoomNotFound />
  }

  return (
    <div class='container container-center column'>
      <h1 class='row center'>
        {
          userIsFacilitator.value
            ? <FacilitatorRoomTitle roomStore={roomStore} />
            : roomStore.room.name
        }
      </h1>
      <div class='row'>
        <RoomControls
          userIsFacilitator={userIsFacilitator}
          roomStore={roomStore}
        />
      </div>
      <div class='row'>
        <MemberUserCards
          roomStore={roomStore}
          user={user}
        />
      </div>
      <div class='row'>
        <VoteCards
          roomStore={roomStore}
          user={user}
        />
      </div>
    </div>
  )
})

export const head: DocumentHead = {
  title: 'Planning poker room',
}
