import { component$, useStylesScoped$, useVisibleTask$ } from '@builder.io/qwik'
import type { DocumentHead } from '@builder.io/qwik-city'
import {
  Form,
  routeAction$,
  routeLoader$,
  useNavigate,
  z,
} from '@builder.io/qwik-city'

import { RoomNotFound } from '~/components/planning-poker/room-not-found'
import { AUTH_COOKIE } from '~/constants/cookies'
import { getDb } from '~/data/db'
import { getPlanningPokerRoomById } from '~/data/planningPokerRoom'
import { addUserToRoom, getMemberByRoomAndUser } from '~/data/planningPokerRoomMember'
import { verifyJwt } from '~/helpers/jwt'
import { getAdminSupabase } from '~/supabase'

import styles from './join.css?inline'

export const useRoom = routeLoader$(async ({ pathname, env, params, cookie, error }) => {
  const jwt = cookie.get(AUTH_COOKIE)?.value
  if (!jwt) {
    console.warn('useRoom', 'no jwt')
    error(401, 'Unauthorized')
    return {
      redirect: '/login',
      returnTo: pathname,
    }
  }

  const adminSupa = getAdminSupabase(env)
  const userResponse = await verifyJwt(adminSupa, jwt)

  if (userResponse.error) {
    console.warn('useRoom', 'bad user', userResponse.error)
    error(401, 'Unauthorized')
    return null
  }

  const roomId = params.roomId
  if (!z.string().uuid().safeParse(roomId).success) {
    console.warn('useRoom', 'bad room id', roomId)
    error(404, 'Room not found')
    return null
  }

  const db = getDb(env)
  const room = await getPlanningPokerRoomById(db, roomId)

  if (!room) {
    console.warn('useRoom', 'no room')
    error(404, 'Room not found')
    return null
  }

  const member = await getMemberByRoomAndUser(db, userResponse.data.user.id, roomId)

  if (member) {
    console.warn('useRoom', 'already joined', `/planning-poker/${roomId}`)
    return { redirect: `/planning-poker/${roomId}` }
  }

  console.log('useRoom', 'success', room)
  return room
})

export const useJoin = routeAction$(
  async (_data, { env, params, cookie, error, redirect }) => {
    const jwt = cookie.get(AUTH_COOKIE)?.value
    if (!jwt) {
      error(400, 'You must be logged in to join a room')
      return
    }

    const adminSupa = getAdminSupabase(env)
    const userResponse = await verifyJwt(adminSupa, jwt)
    if (userResponse.error) {
      error(401, 'Unauthorized')
      return
    }

    const roomId = params.roomId
    const db = getDb(env)
    const room = await getPlanningPokerRoomById(db, roomId)

    if (!room) {
      error(404, 'Room not found')
      return
    }

    await addUserToRoom(db, userResponse.data.user.id, roomId)

    redirect(307, `/planning-poker/${params.roomId}`)
  },
)

export default component$(() => {
  useStylesScoped$(styles)
  const room = useRoom()
  const action = useJoin()
  const navigate = useNavigate()
  // eslint-disable-next-line qwik/no-use-visible-task
  useVisibleTask$(() => {
    if (room.value) {
      if ('returnTo' in room.value && room.value.returnTo) {
        sessionStorage.setItem('returnTo', room.value.returnTo)
      }

      if ('redirect' in room.value) {
        navigate(room.value.redirect)
      }
    }
  }, { strategy: 'document-ready' })

  if (!room.value) {
    return <RoomNotFound />
  }

  if ('redirect' in room.value) {
    return null
  }

  return (
    <div class='container container-center'>
      <Form class='form' action={action}>
        <h1>
          <span class='highlight'>{room.value.name}</span>
        </h1>
        <h2>You're about to join this room</h2>
        <button type='submit'>Join Room</button>
      </Form>
    </div>
  )
})

export const head: DocumentHead = {
  title: 'Join a room',
}
