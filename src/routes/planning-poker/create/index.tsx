import { component$, useStylesScoped$ } from '@builder.io/qwik'
import type { DocumentHead } from '@builder.io/qwik-city'
import { Form, routeAction$, z, zod$ } from '@builder.io/qwik-city'

import { FloatingLabelInput } from '~/components/floating-label-input'
import { AUTH_COOKIE } from '~/constants/cookies'
import { getDb } from '~/data/db'
import { createPlanningPokerRoom } from '~/data/planningPokerRoom'
import { addUserToRoom } from '~/data/planningPokerRoomMember'
import { verifyJwt } from '~/helpers/jwt'
import { getAdminSupabase } from '~/supabase'

import styles from './create.css?inline'

export const nameKey = 'name'

export const useCreate = routeAction$(
  async ({ name }, { env, cookie, error, redirect }) => {
    const jwt = cookie.get(AUTH_COOKIE)?.value
    if (!jwt) {
      error(400, 'You must be logged in to join a room')
      return
    }

    const adminSupa = getAdminSupabase(env)
    const userResponse = await verifyJwt(adminSupa, jwt)
    if (userResponse.error) {
      error(401, 'Unauthorized')
      return
    }

    const db = getDb(env)
    const room = await createPlanningPokerRoom(db, name, userResponse.data.user)
    await addUserToRoom(db, userResponse.data.user.id, room.id)

    redirect(307, `/planning-poker/${room.id}`)
  },
  zod$({
    [nameKey]: z.string().min(2),
  }),
)

export default component$(() => {
  useStylesScoped$(styles)
  const action = useCreate()

  return (
    <div class='container container-center'>
      <Form class='form' action={action}>
        <h1>
          <span class='highlight'>Create</span> a Room
        </h1>
        <FloatingLabelInput label='Room Name' name={nameKey} />
        <button type='submit'>Create Room</button>
      </Form>
    </div>
  )
})

export const head: DocumentHead = {
  title: 'Create a room',
}
