import {
  $,
  component$,
  useComputed$,
  useSignal,
  useStylesScoped$,
} from '@builder.io/qwik'
import type { DocumentHead } from '@builder.io/qwik-city'
import { useNavigate, z } from '@builder.io/qwik-city'

import { FloatingLabelInput } from '~/components/floating-label-input'
import { FormError } from '~/components/form-error'
import { GoogleButton } from '~/components/google-button'
import { PasswordStrength } from '~/components/password-strength'
import { getPasswordStrength } from '~/helpers/passwords'
import { supabase } from '~/supabase'

import styles from './signup.css?inline'

const emailKey = 'email'
const passwordKey = 'password'
const verifyPasswordKey = 'verifyPassword'

export const stateChecker = z
  .object({
    email: z
      .string({
        required_error: 'Please enter a valid email.',
      })
      .email({
        message: 'Please enter a valid email.',
      }),
    password: z
      .string({
        required_error: 'Please enter a password.',
      })
      .refine((pass) => getPasswordStrength(pass) >= 2, {
        message:
          'Your password is too weak.  Try using a longer password with upper and lowercase letters, numbers, and symbols',
      }),
    verifyPassword: z.string(),
  })
  .refine(({ password, verifyPassword }) => password === verifyPassword, {
    message: 'Passwords must match.',
  })

export default component$(() => {
  const navigate = useNavigate()
  const passwordErrors = useSignal<string[]>([])
  const verifyPasswordErrors = useSignal<string[]>([])
  const formErrors = useSignal<string[]>([])
  useStylesScoped$(styles)

  const passwordSignal = useSignal('')
  const passwordStrength = useComputed$(() => {
    return getPasswordStrength(passwordSignal.value)
  })

  const signUpUser = $(async ({ formData }: { formData: FormData }) => {
    const email = formData.get(emailKey) as string
    const password = formData.get(passwordKey) as string
    const verifyPassword = formData.get(verifyPasswordKey) as string

    const result = stateChecker.safeParse({
      email,
      password,
      verifyPassword,
    })

    if (!result.success) {
      passwordErrors.value = result.error.formErrors.fieldErrors.password ?? []
      verifyPasswordErrors.value =
        result.error.formErrors.fieldErrors.verifyPassword ?? []
      formErrors.value = result.error.formErrors.formErrors
      return
    }

    // Clear all errors
    passwordErrors.value = []
    verifyPasswordErrors.value = []
    formErrors.value = []

    const resp = await supabase.auth.signUp({
      email,
      password,
    })

    if (resp.error) {
      formErrors.value = [resp.error.message]
    } else if (resp.data.user) {
      navigate(sessionStorage.getItem('returnTo') ?? '/')
      sessionStorage.removeItem('returnTo')
    }
  })

  return (
    <div class='container container-center' data-testid='signup-page'>
      <form
        class='form'
        preventdefault:submit
        onSubmit$={async (_ev, form) => {
          await signUpUser({ formData: new FormData(form) })
        }}
      >
        <FloatingLabelInput
          label='Email'
          type='email'
          name={emailKey}
          autoComplete='email'
          required
        />
        <FloatingLabelInput
          label='Password'
          type='password'
          name={passwordKey}
          autoComplete='new-password'
          required
          errors={passwordErrors.value}
          bind:value={passwordSignal}
        >
          <PasswordStrength value={passwordStrength.value} />
        </FloatingLabelInput>
        <FloatingLabelInput
          label='Verify Password'
          type='password'
          name={verifyPasswordKey}
          autoComplete='new-password'
          required
          errors={verifyPasswordErrors.value}
        />
        {formErrors.value.map((err, idx) => (
          <FormError key={idx}>{err}</FormError>
        ))}
        <button type='submit'>Create User</button>
      </form>

      <GoogleButton />
    </div>
  )
})

export const head: DocumentHead = {
  title: 'Sign Up',
}
