import type { DataTable } from '@badeball/cypress-cucumber-preprocessor'
import { Given, Step, When } from '@badeball/cypress-cucumber-preprocessor'

import { generateEmail } from '~/test-utils/email'

Given('I am a new user', function () {
  Step(this, 'I am on the signup page')
  Step(this, 'I sign up new user information')
})

Given('I am on the signup page', function () {
  cy.visit('/signup')

  cy.findByTestId('signup-page').should('be.visible')
})

When('I sign up new user information', function (data?: DataTable) {
  const rows = data?.rowsHash()
  
  this.e2eCtx = {
    ...this.e2eCtx,
    email: rows?.email ?? generateEmail(),
    password: rows?.password ?? 'Insecure P@ssw0rd',
  }

  cy.findByLabelText('Email').type(this.e2eCtx.email!)
  cy.findByLabelText('Password').type(this.e2eCtx.password!)
  cy.findByLabelText('Verify Password').type(rows?.verifyPassword ?? this.e2eCtx.password!)

  cy.findByText('Create User').click()
})
