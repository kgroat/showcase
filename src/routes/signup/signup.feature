Feature: Sign up page

  Scenario: As a new user, I can create a new account
    Given I am on the signup page
    When I sign up new user information
    Then I am on the home page
    And I should be logged in

  Scenario Outline: As a new user, I should have strong credentials
    Given I am on the signup page
    When I sign up new user information
      | password       | <password>       |
      | verifyPassword | <verifyPassword> |
    Then I should see a "<error>" form error

    Examples:
        | password        | verifyPassword | error                      |
        | weak            | weak           | Your password is too weak  |
        | Strong P@ssw0rd | does not match | Passwords must match       |
