import { component$, useVisibleTask$ } from '@builder.io/qwik'
import type { DocumentHead, RequestHandler } from '@builder.io/qwik-city'
import { routeLoader$, useNavigate, z } from '@builder.io/qwik-city'
import { mediaType } from '@hapi/accept'
import { sha256 } from 'js-sha256'

import { AUTH_COOKIE } from '~/constants/cookies'
import { verifyJwt } from '~/helpers/jwt'
import { getUserDisplayName } from '~/helpers/user'
import anonymousUser from '~/media/anonymous_user.png'
import { getAdminSupabase } from '~/supabase'

const HTML_MIME_TYPE = 'text/html'
const JSON_MIME_TYPE = 'application/json'

export const onGet: RequestHandler = async ({ env, params, json, text, cookie, request }) => {
  const acceptHeader = request.headers.get('accept') ?? 'text/html'
  const preferredEncoding = mediaType(acceptHeader, [HTML_MIME_TYPE, JSON_MIME_TYPE])
  if (preferredEncoding === HTML_MIME_TYPE) {
    return
  } else if (preferredEncoding !== JSON_MIME_TYPE) {
    text(406, '')
    return
  }

  const { id } = params
  const jwt = cookie.get(AUTH_COOKIE)?.value
  const adminSupa = getAdminSupabase(env)

  if (id === 'me') {
    if (!jwt) {
      text(404, '')
      return
    }
    const payload = await verifyJwt(adminSupa, jwt)
    if (payload.error) {
      text(404, '')
      return
    }

    json(200, payload.data.user)
    return
  }

  if (!z.string().uuid().safeParse(id).success) {
    console.warn('/user/[id] onGet', 'bad user id', id)
    text(404, '')
    return
  }

  const userResponse = await adminSupa.auth.admin.getUserById(id)

  if (userResponse.error) {
    text(404, '')
    return
  }

  json(200, userResponse.data.user)
}

export const useUserData = routeLoader$(async ({ params, env, cookie }) => {
  const adminSupa = getAdminSupabase(env)
  const { id } = params
  const jwt = cookie.get(AUTH_COOKIE)?.value

  if (id === 'me') {
    if (!jwt) {
      return { redirect: '/login' }
    }
    const payload = await verifyJwt(adminSupa, jwt)
    if (payload.error) {
      return null
    }
    return {
      user: payload.data.user,
      isMe: true,
    }
  }

  if (!z.string().uuid().safeParse(params.id).success) {
    console.warn('/user/[id] onGet', 'bad user id', params.id)
    return null
  }

  const response = await adminSupa.auth.admin.getUserById(id)
  if (response.error) {
    console.log('useUserData error', response.error)
    return null
  }

  return {
    user: response.data.user,
    isMe: false,
  }
})

export default component$(() => {
  const navigate = useNavigate()
  const userData = useUserData()

  // eslint-disable-next-line qwik/no-use-visible-task
  useVisibleTask$(
    () => {
      if (userData.value && 'redirect' in userData.value) {
        navigate(userData.value.redirect)
      }
    },
    {
      strategy: 'document-ready',
    },
  )

  if (!userData.value || 'redirect' in userData.value) {
    return (
      <div class='container container-center'>
        <h1>
          <span class='highlight'>404</span> User Not Found
        </h1>
        <h2>The user you are looking for does not exist.</h2>
      </div>
    )
  }

  const { user } = userData.value

  const emailHash = user.email ? sha256.hex(user.email) : ''
  const displayName = getUserDisplayName(user)
  const email = user.email

  return (
    <div class='container container-center'>
      <img
        src={
          emailHash
            ? `https://www.gravatar.com/avatar/${emailHash}?s=150`
            : anonymousUser
        }
        width={150}
        height={150}
        style={{ borderRadius: '75px' }}
        crossOrigin='anonymous'
        />
      <h1>
        <span class='highlight'>{displayName}</span>
      </h1>
      { email && email !== displayName && <p>{email}</p> }
      <p>
        <span class='highlight'>Joined:</span>{' '}
        {new Date(user.created_at).toLocaleDateString()}
      </p>
      <p>
        <span class='highlight'>Last seen:</span>{' '}
        {user.last_sign_in_at
          ? new Date(user.last_sign_in_at).toLocaleDateString()
          : 'Never'}
      </p>
    </div>
  )
})

export const head: DocumentHead = ({ resolveValue }) => {
  const userData = resolveValue(useUserData)
  let title: string

  if (userData) {
    if ('redirect' in userData) {
      title = 'Redirecting...'
    } else if (userData.isMe) {
      title = 'My profile'
    } else {
      title = `User info (${getUserDisplayName(userData.user)})`
    }
  } else {
    title = 'User not found'
  }

  return {
    title,
  }
}
