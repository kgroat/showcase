import type { EnvGetter } from '@builder.io/qwik-city/middleware/request-handler'
import { createClient } from '@supabase/supabase-js'

import { SUPA_AUTH_KEY } from '~/constants/localStorage'
import type { Database } from '~/supabase/types'

export function isUserLoggedIn() {
  return !!localStorage.getItem(SUPA_AUTH_KEY)
}

export const SUPABASE_URL = import.meta.env.PUBLIC_SUPABASE_URL
export const SUPABASE_KEY = import.meta.env.PUBLIC_SUPABASE_KEY

export const supabase = createClient<Database>(
  SUPABASE_URL,
  SUPABASE_KEY,
  {
    auth: {
      storageKey: SUPA_AUTH_KEY,
    },
  },
)

if (typeof window === 'object') {
  (window as any).supabase = supabase
}

export const getAdminSupabase = (env: EnvGetter) => {
  return createClient(SUPABASE_URL, env.get('SUPABASE_PRIVATE_KEY')!, {
    auth: {
      persistSession: false,
    },
  })
}
