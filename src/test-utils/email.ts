
export const DEFAULT_NONCE_LENGTH = 6

export function emailNonce (length = DEFAULT_NONCE_LENGTH) {
  const nowStr = Date.now().toString(10)

  return nowStr.substring(nowStr.length - length)
}

export function generateEmail (length = DEFAULT_NONCE_LENGTH) {
  return `planning+${emailNonce(length)}@kgroat.dev`
}
