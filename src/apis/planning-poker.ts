import { supabase } from '~/supabase'

export const setOwnVote = async (roomId: string, userId: string, vote: string | null, controller?: AbortController): Promise<string | null> => {
  let call = supabase
    .from('planning_poker_room_member')
    .update({ vote })
    .eq('room_id', roomId)
    .eq('user_id', userId)
    .select('vote')

  if (controller) {
    call = call.abortSignal(controller.signal)
  }

  const { error, data } = await call.single()

  if (error) {
    console.warn('Failed to set vote', error)
    throw new Error(error.message)
  }

  return data.vote
}

export const toggleShowVotes = async (roomId: string, show: boolean, controller?: AbortController): Promise<boolean> => {
  console.log('toggleShowVotes')
  let call = supabase
    .from('planning_poker_room')
    .update({ votes_visible: show })
    .eq('id', roomId)
    .select('votes_visible')

  if (controller) {
    call = call.abortSignal(controller.signal)
  }

  const { error, data } = await call.single()

  if (error) {
    console.warn('Failed to toggle votes', error)
    throw new Error(error.message)
  }

  console.log('vote visibility toggled')

  return data.votes_visible
}

export const clearVotes = async (roomId: string, controller?: AbortController): Promise<void> => {
  console.log('clearVotes')
  let call = supabase
    .from('planning_poker_room_member')
    .update({ vote: null })
    .eq('room_id', roomId)

  if (controller) {
    call = call.abortSignal(controller.signal)
  }

  const { error } = await call

  if (error) {
    console.warn('Failed to clear votes', error)
    throw new Error(error.message)
  }

  console.log('votes cleared')
}
